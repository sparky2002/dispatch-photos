#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import sys
import os
import exifread
import shutil
from datetime import datetime

#import exiv2
# exiv2 is pretty much unusable as it's barely documented

# Commandline argument parser
#
parser = argparse.ArgumentParser(
    description="Dispatch RAW photo files from source (recursive) into subdirectories of target according to image date")

EXIF_TIMESTAMP = "EXIF DateTimeOriginal"
# EXIF_IMAGEFMT = "MakerNote RecordMode"

# filetree is a dictionary indexed on YYYYMMDD gathered from EXIF data in source files,
# corresponding to target directories to be created.
# Under each key is a list of dictionaries  comprised of relevant image data from EXIF data:
# source file name
# YYYYMMDD, HHMMSS, camera S/N
# Timestamp info is from EXIF_TIMESTAMP tag
# Camera serial is from EXIF_CAMERASN tag
#
filetree = {}

# header labels and their size for formatting in print_tree/print_select_tags
#
select_headers = {
    "f_source": { "text": "source", "len": 40 },
    "ts_date" : { "text": "date", "len": 8 },
    "ts_time" : { "text": "time", "len": 8 },
    "cam_sn"  : { "text": "cam", "len": 12 }
}

# collection summary counters
#
global grp_cnt, img_cnt

# commandline arguments parsed
global args

# print out output
#
def log(str):
    ts = datetime.now()
    print( "%s [exif_dispatch] %s" % (ts, str) )

# Exit on error
#
def abort(level, text):
    log( text )
    exit( level )
    
# Print all tags from an image. This is called with the tags dictionary
# reported by exifread as the argument.
def print_all_tags(taglist):
    for k in taglist.keys():
        print( "%s: %s" % (k, taglist[k]) )
    print( "===" )

# Print chosen tags (called from print_tree)
#
def print_select_tags(taglist, fmt):
    #for k in [ "f_source", "cam_sn", "ts_date", "ts_time" ]:
    #    print( "%8s 🠆 %s" % (k, taglist[k]) )
    hd_format = []
    hd_data = []
    hd_header = []
    for k in select_headers.keys():
        hd_data.append( taglist[k] )
                          
    print( fmt % tuple(hd_data) )

# Pretty print collected data. Mostly used for debugging.
#
def print_tree(data_tree):
    hd_format = []
    hd_header = []
    for hk in select_headers.keys():
        hd_format.append( "%%%ds" % select_headers[hk]["len"] )
        hd_header.append( select_headers[hk]["text"] )
    hd_format_str = " ".join(hd_format)
                          
    for dk in data_tree.keys():
        print(dk)
        for info in filetree[dk]:
            print_select_tags(info, hd_format_str)

# Walk the tree of collected target dirs and files and copy them to their
# destination.
def move_files(tree, tgt_d):
    tgt_path = os.path.expandvars(tgt_d)

    if vars(args)["r"]:
        log("Option 'r' is not implemented at this time")
        
    for tgt_subdir in tree.keys():
        valid_target_d = True
        subdir_path = os.path.join( tgt_path, tgt_subdir.replace(":", "-") )

        # Create new subdir under target, if needed
        if not os.path.isdir(subdir_path):
            log( "No directory %s" % subdir_path )
            if not vars(args)["n"]:
                # skip it if dry run
                try:
                    log( "Creating %s" % subdir_path )
                    os.mkdir(subdir_path)
                except OSError as error:
                    valid_target_d = False
                    log(error)
                    log("IMAGES WILL NOT BE TRANSFERRED")
        else:
            log( "Directory %s found" % subdir_path )

        # Perform file rename/copy
        if valid_target_d:
            for fileinfo in tree[tgt_subdir]:
                fname, fext = os.path.splitext( fileinfo["f_source"] )
                tgt_name = "%s-%s-%s%s" % (
                    fileinfo["ts_date"].replace(":", ""),
                    fileinfo["ts_time"].replace(":", ""),
                    fileinfo["cam_sn"],
                    fext
                )
                msg = "%s 🠆 %s" % (fileinfo["f_source"], tgt_name)
                if not vars(args)["n"]:
                    shutil.copy( fileinfo["f_source"], os.path.join(subdir_path, tgt_name) )
                else:
                    msg = "DRY RUN - %s" % msg

                log(msg)

# Insert collected image data in the filetree. Selected EXIF fields are passed
# as a dictionary in info_list, among those the image date is used as key to the filetree.
def insert_in_tree(tree, info_list):
    global img_cnt, grp_cnt
    dtstr = "date"
    
    if info_list is not None:
        img_cnt += 1
        fld = info_list["ts_date"]
        src = info_list["f_source"]
        try:
            tree[fld].append(info_list)
        except KeyError:
            grp_cnt += 1
            if grp_cnt > 1:
                dtstr = "dates"
            tree[fld] = [ info_list ]

        print( "Processed %4d images from %2d %s" % (img_cnt, grp_cnt, dtstr), end="\r" )

# Grab EXIF data from given image. Return selected fields as a dictionary.
#
def exif_fetch(src_f):
    # log( "fetch %s" % src_f )
    with open(src_f, "rb") as infile:
        tags = exifread.process_file(infile, details=True)
    # log( "tags length: %d" % len(tags) )
         
    if len(tags) > 0:
        if vars(args)["v"]:
            print( src_f )
            print_all_tags(tags)

        t_dt, t_hr = tags[EXIF_TIMESTAMP].values.split(' ')
        # All cameras don't store their S/N under the same tag, even within
        # the same brand. JPG and CR2 differ as well.
        cam_sn = "unknown"
        for sntag in [ "MakerNote InternalSerialNumber",
                       "MakerNote SerialNumber",
                       "EXIF BodySerialNumber" ]:
            try:
                cam_sn = tags[sntag]
            except KeyError:
                continue

        file_info = {
            "f_source": src_f,
            # "f_format": tags[EXIF_IMAGEFMT],
            "ts_date": t_dt,
            "ts_time": t_hr,
            "cam_sn": cam_sn
        }
        return file_info
    
# Walk source directory recursively, ignoring the tree structure, and collect all files underneath.
# The files will be dispatched further down in the process based on the EXIF image creation date.
def collect_files(src_d):
    for cf_root, cf_dirs, cf_files in os.walk(src_d):
        for f in cf_files:
            insert_in_tree(filetree, exif_fetch( os.path.join(cf_root, f) ))
    print()

img_cnt = grp_cnt = 0            
parser.add_argument('src', help='Source directory')
parser.add_argument('dst', help='Target directory')
parser.add_argument('-n', action='store_true', help='dry run')
parser.add_argument('-r', action='store_true', help='remove files from src if specified')
parser.add_argument('-v', action='store_true', help='verbose')
args = parser.parse_args( sys.argv[1:] )

src_dir = vars(args)['src']
dst_dir = vars(args)['dst']
for d in [ src_dir, dst_dir ]:
    # Check that directory exist and is writable
    if not os.path.isdir(d):
        abort( 2, "%s is not a valid directory" % d)
    else:
        if not os.access(d, os.W_OK):
            abort( 3, "Directory %s in not writable" )

collect_files(os.path.expandvars(src_dir))

if vars(args)["v"]:
    print_tree( filetree )

move_files(filetree, dst_dir)
