#! /usr/bin/env python3

# Display, in tabular form, all the EXIF tags encountered in all provided files
# and their values. Not all tags are available in all files (e.g. different camera manufacturers
# don't necessarily report the same tags, and MakerNote tags are by definition mfg-specific)

import sys
import argparse
import exifread
import os

parser = argparse.ArgumentParser(
    description="Show image EXIF data from specified files side-by-side")
parser.add_argument('-t', help='Maximum tag description length', type=int, default=50)
parser.add_argument('-l', help='Maximum tag value length', type=int, default=20)
parser.add_argument('pics', nargs='*', help='Files to process')
args = parser.parse_args( sys.argv[1:] )

all_tags = {}        # complete dictionary of encountered tags
valid_files = []     # only valid files from argument list
max_tag_desc_len = 0 # longest tag description actually encountered

# print to STDERR
#
def print_error(str):
    print(str, file=sys.stderr)

# shorten string to specified length, put an ellipsis in the middle
#
def shorten_string(str, maxl):
    clen = int(maxl / 2) - 1
    if len(str) > maxl:
        return str[:clen] + '…' + str[-clen:]
    else:
        return str

# start processing files list
#
for f in vars(args)['pics']:
    try:
        with open(os.path.expandvars(os.path.expanduser(f)), "rb") as infile:
            valid_files.append(f)
            tags = exifread.process_file(infile)
        
            for t in tags:
                max_tag_desc_len = max(max_tag_desc_len, len(t))
                try:
                    all_tags[t][f] = tags[t]
                except KeyError:
                    all_tags[t] = { f: tags[t] }
                    
    except FileNotFoundError:
        print_error("File not found: %s" % f)

# set maximum string lengths for display
#
tagfmt = "%%%ds" % min(vars(args)['t'], max_tag_desc_len)
strfmt = "%%%ds" % vars(args)['l']

# print header with file name at each column
#
if len(valid_files) > 0:
    print(tagfmt % "", end=" :")
    for f in valid_files:
        print(strfmt % os.path.basename(f), end="|")
    print()

# go over full list of tags and display encountered value if any
#
for k in sorted(all_tags.keys()):
    print(tagfmt % shorten_string(k, vars(args)['t']), end=" :")
    for f in valid_files:
        try:
            strin = str(all_tags[k][f])
        except KeyError:
            strin = "-"

        if k == "JPEGThumbnail":
            strin = "<ignored>"
            
        print(strfmt % shorten_string(strin, vars(args)['l']), end="|")
    print()
            
